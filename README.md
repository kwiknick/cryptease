# CryptEase
---

CryptEase is a Windows Universal Platform application that will make it easier for basic encryption of files.  This is a work in progress, but will attempt to make it common place to encrypt any sensitive data that one would want to protect.

## License
---

[MIT License]

[MIT License]: <https://opensource.org/licenses/MIT>
